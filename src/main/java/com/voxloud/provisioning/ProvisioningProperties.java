package com.voxloud.provisioning;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("")
public class ProvisioningProperties {

    private final Map<String, String> provisioning = new HashMap<>();

    public Map<String, String> getProvisioning() {
        return provisioning;
    }

}