package com.voxloud.provisioning.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.voxloud.provisioning.service.ProvisioningService;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {
	
	@Autowired
	private ProvisioningService service;
	
	private static final String FETCH_CONFIG = "/provisioning/{macAddress}";

	@GetMapping(FETCH_CONFIG)
	public ResponseEntity<String> getConfiguration(@PathVariable String macAddress){
		
		return Optional.ofNullable(service.getProvisioningFile(macAddress))
				.map(response->new ResponseEntity<>(response, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.UNAUTHORIZED));
	} 
}