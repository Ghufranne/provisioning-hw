package com.voxloud.provisioning.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class PropertiesParser implements ConfigParser {

	@Override
	public Map<String, String> parseToMap(String overrideFragment) {
		Map<String, String> configMap = Optional.ofNullable(overrideFragment)
				.map(frg->{
						Map<String, String> configMapLocal = Arrays.stream(frg.split("\\n"))
							.map(s -> s.split("="))
							.collect(Collectors.toMap(
									a -> a[0],  
									a -> a[1]  
									));
							return configMapLocal;
						}).orElse(Collections.emptyMap());

		return configMap;
	}

	@Override
	public String parseToString(Map<String, String> map) {
		String propStr = map.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining(System.lineSeparator()));
		return propStr;
	}

}
