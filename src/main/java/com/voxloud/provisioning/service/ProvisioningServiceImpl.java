package com.voxloud.provisioning.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.voxloud.provisioning.ProvisioningProperties;
import com.voxloud.provisioning.repository.DeviceRepository;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {

	@Autowired
	DeviceRepository repository;
	
	@Autowired
	ConfigParserFactory parserFactory;
	
	@Autowired
	ProvisioningProperties provisioningProperties;
	
    public String getProvisioningFile(String macAddress) {
    	
    	String config = repository.findById(macAddress).map(d->{
    		ConfigParser parser = parserFactory.getParser(d.getModel());
    		Map<String, String> fragmentMap = parser.parseToMap(d.getOverrideFragment());
    		Map<String, String> configMap = new HashMap<>(provisioningProperties.getProvisioning());
    		fragmentMap.forEach((k,v)->configMap.merge(k, v, (v1,v2)->v2));
    		configMap.put("username", d.getUsername());
    		configMap.put("password", d.getPassword());
    		
    		return parser.parseToString(configMap);
    	}).orElse(null);
    	return config;
    }
}
