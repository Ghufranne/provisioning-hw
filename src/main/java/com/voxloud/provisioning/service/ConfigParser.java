package com.voxloud.provisioning.service;

import java.util.Map;

public interface ConfigParser {

	Map<String, String> parseToMap(String overrideFragment);
	
	String parseToString(Map<String, String> map);
}
