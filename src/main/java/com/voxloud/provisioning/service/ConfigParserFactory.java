package com.voxloud.provisioning.service;

import org.springframework.stereotype.Component;

import com.voxloud.provisioning.entity.Device.DeviceModel;

@Component
public class ConfigParserFactory {
	
	ConfigParser getParser(DeviceModel model){
		if(model == DeviceModel.CONFERENCE)
			return new JsonParser();
		if(model == DeviceModel.DESK)
			return new PropertiesParser();
		return null;
	}
}
