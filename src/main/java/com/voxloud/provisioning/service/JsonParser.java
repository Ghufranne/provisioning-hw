package com.voxloud.provisioning.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser implements ConfigParser {

	@Override
	public Map<String, String> parseToMap(String overrideFragment) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			TypeReference<HashMap<String,String>> typeRef = new TypeReference<HashMap<String,String>>() {};
			return overrideFragment!=null ? objectMapper.readValue(overrideFragment, typeRef): Collections.emptyMap();
		} catch (JsonProcessingException e) {
			return Collections.emptyMap();
		}
	}

	@Override
	public String parseToString(Map<String, String> map) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			return "Some error occured";
		}
	}

}
